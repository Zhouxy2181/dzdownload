package models

import "time"

type DbInsKeyInfo struct {
	InsIdCd    string    `xorm:"varchar(64) pk notnull 'INS_ID_CD'"`
	KeyId      string    `xorm:"varchar(64) pk notnull 'KEY_ID'"`
	RsaPrivKey string    `xorm:"varchar(2048) 'RSA_PRIV_KEY'"`
	RsaPubKey  string    `xorm:"varchar(2048) 'RSA_PUB_KEY'"`
	KeyIndex   int       `xorm:"integer 'KEY_INDEX'"`
	ZmkLen     int       `xorm:"integer 'ZMK_LEN'"`
	Zmk        int       `xorm:"varchar(49) 'ZMK'"`
	TrkLen     int       `xorm:"int 'TRK_LEN'"`
	Trk        string    `xorm:"varchar(49) 'TRK'"`
	MakLen     int       `xorm:"int 'MAK_LEN'"`
	Mak1       string    `xorm:"varchar(49) 'MAK_1'"`
	Mak2       string    `xorm:"varchar(49) 'MAK_2'"`
	MakChk     string    `xorm:"varchar(49) 'MAK_CHK'"`
	PikLen     int       `xorm:"int 'PIK_LEN'"`
	Pik1       string    `xorm:"varchar(49) 'PIK_1'"`
	Pik2       string    `xorm:"varchar(49) 'PIK_2'"`
	PikChk     string    `xorm:"varchar(49) 'PIK_CHK'"`
	Misc1      string    `xorm:"varchar(2048) 'MISC_1'"`
	Misc2      string    `xorm:"varchar(2048) 'MISC_2'"`
	RecUpdTs   time.Time `xorm:"'REC_UPD_TS'"`
	RecCrtTs   time.Time `xorm:"'REC_CRT_TS'"`
}

func (t DbInsKeyInfo) TableName() string {
	return "TBL_INS_KEY_INF"
}
