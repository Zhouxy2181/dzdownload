package main

import (
	"dzDownload/modules/app"
	"dzDownload/modules/dbxorm"
	"dzDownload/modules/log"
	"dzDownload/modules/server"
	"flag"
	"fmt"
	"os"
	"runtime"
)

var dzCfgFile = flag.String("conF", "config/stg/dzDownload.yml", "config file")

const DEFAULT = "default"

func main() {
	flag.Parse()

	fmt.Println("配置文件：", *dzCfgFile)
	/*加载日志*/
	err := log.LogInit(*dzCfgFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "配置文件[%s]加载失败:%s", *dzCfgFile, err)
		return
	}
	/*日志加载*/
	lg := log.NewLogger(DEFAULT)
	lg.Info("日志加载成功")

	/*数据库加载*/
	err = dbxorm.DbInit(*dzCfgFile)
	if err != nil {
		lg.Error("数据库加载失败", err)
		return
	}

	/*服务加载*/
	svr, err := server.SvrInit(*dzCfgFile)
	if err != nil {
		lg.Error("服务加载失败", err)
		return
	}
	/*应用加载*/
	app, err := app.AppInit(*dzCfgFile)
	if err != nil {
		lg.Error("应用加载失败", err)
	}
	lg.Info("应用服务加载成功")

	svr.AddHandle(svr.ServerUrl, app, "POST")

	lg.Info("应用服务启动成功......")
	svr.Run()

	runtime.Goexit()
}
