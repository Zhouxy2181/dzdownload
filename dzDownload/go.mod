module dzDownload

go 1.12

require (
	github.com/axgle/mahonia v0.0.0-20180208002826-3358181d7394
	github.com/go-sql-driver/mysql v1.5.0
	github.com/go-xorm/xorm v0.7.9
	github.com/julienschmidt/httprouter v1.3.0
	github.com/mattn/go-oci8 v0.0.7
	github.com/sirupsen/logrus v1.4.2
	gitlab.yietech.com/zzb0905/logrus_mate v1.2.2
	gopkg.in/yaml.v2 v2.2.2
	sourcegraph.com/sourcegraph/appdash v0.0.0-20180531100431-4c381bd170b4
	xorm.io/core v0.7.2-0.20190928055935-90aeac8d08eb
)
