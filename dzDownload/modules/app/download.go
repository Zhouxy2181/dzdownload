package app

import (
	"dzDownload/models"
	"dzDownload/modules/dbxorm"
	"dzDownload/modules/security"
	"encoding/json"
	"fmt"
	"github.com/axgle/mahonia"
	"io/ioutil"
	"path/filepath"
)

type ReqMsg struct {
	Version    string `json:"version"`
	InsIdCd    string `json:"ins_id_cd"`
	ChnInsIdCd string `json:"chn_ins_id_cd"`
	FileDate   string `json:"file_date"`
	Signature  string `json:"signature"`
}

func (t *BaseApp) DownloadDzFile(msg []byte) error {
	var err error
	var ok bool

	reqmsg := &ReqMsg{}
	err = json.Unmarshal(msg, reqmsg)
	if err != nil {
		t.lg.Error("unmarshal请求报文失败", err)
		return err
	}
	resData := fmt.Sprintf("version=%s&ins_id_cd=%s&chn_ins_id_cd=%s&file_date=%s",
		reqmsg.Version, reqmsg.InsIdCd, reqmsg.ChnInsIdCd, reqmsg.FileDate)
	t.lg.Info("待验签原串：", resData)

	//DB
	insKeyInf := new(models.DbInsKeyInfo)
	dbc := dbxorm.GetDbInstance()
	ok, err = dbc.SQL("select * from TBL_INS_KEY_INF where ins_id_cd = ? ",
		reqmsg.ChnInsIdCd).Get(insKeyInf)
	if err != nil {
		t.lg.Error("获取密钥信息失败", err)
		return err
	} else if !ok {
		t.lg.Error("密钥信息不存在", ok)
		return nil
	}

	keystr, err := security.NewRsaInMap(insKeyInf.RsaPrivKey, insKeyInf.RsaPubKey)
	if err != nil {
		t.lg.Error("获取密钥失败", err)
		return err
	}
	ok, err = keystr.Verfy(resData, reqmsg.Signature)
	if !ok {
		t.lg.Error("验证签名失败", err)
		return err
	}

	dir, err := filepath.Abs(t.DzFileDir)
	if err != nil {
		t.lg.Error("获取对账文件目录失败", err)
		return err
	}
	fname := "C_" + reqmsg.InsIdCd + "_" + reqmsg.FileDate + ".txt"
	fpath := dir + "/" + reqmsg.FileDate + "/" + fname
	fbytes, err := ioutil.ReadFile(fpath)
	if err != nil {
		t.lg.Error("读取对账文件内容失败", err)
		return err
	}

	var rspmsg = make(map[string]string)
	rspmsg["version"] = reqmsg.Version
	rspmsg["ins_id_cd"] = reqmsg.InsIdCd
	rspmsg["chn_ins_id_cd"] = reqmsg.ChnInsIdCd
	rspmsg["file_date"] = reqmsg.FileDate
	rspmsg["file_name"] = fname
	rspmsg["file_datas"] = string(GBKtUTF8(fbytes))
	t.RspMsg, err = json.Marshal(rspmsg)
	if err != nil {
		t.lg.Error("打包应答报文失败", err)
		return err
	}

	return nil

	//f, err := os.Open(fpath)
	//if err != nil {
	//	t.lg.Error("打开对账文件失败", err)
	//	return err
	//}
	//defer f.Close()
	//msgbuf := &bytes.Buffer{}
	//msgwrite := multipart.NewWriter(msgbuf)
	//fwrite, err := msgwrite.CreateFormFile("file_content", filepath.Base(fname))
	//if err != nil {
	//	t.lg.Error("组装文件失败", err)
	//	return err
	//}
	//_, err = io.Copy(fwrite, f)
	//if err != nil {
	//	t.lg.Error("写文件失败", err)
	//	return err
	//}
	//err = msgwrite.WriteField("version", reqmsg.Version)
	//err = msgwrite.WriteField("ins_id_cd", reqmsg.InsIdCd)
	//err = msgwrite.WriteField("chn_ins_id_cd", reqmsg.ChnInsIdCd)
	//err = msgwrite.WriteField("file_date", reqmsg.FileDate)
	//if err != nil {
	//	t.lg.Error("生成报文失败", err)
	//	return err
	//}
	//t.ContentType = msgwrite.FormDataContentType()
	//t.RspMsg = msgbuf
	//defer msgwrite.Close()

	//return nil
}

/*GBK 转码到 UTF8*/
func GBKtUTF8(src []byte) (dst []byte) {
	u2g := mahonia.NewDecoder("GBK")
	tmp := u2g.ConvertString(string(src))
	return []byte(tmp)
}
