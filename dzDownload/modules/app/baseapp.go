package app

import (
	"dzDownload/modules/log"
	"dzDownload/modules/security"
	"dzDownload/modules/yamlcfg"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"net/http"
	"time"
)

type AppCfg struct {
	DzFileDir string            `yaml:"DZFILEDIR"`
	SignKey   map[string]string `yaml:"SIGNKEY"`
}
type BaseApp struct {
	*AppCfg `yaml:"APPCFG"`
	SignKey *security.RSAKey
	lg      *log.LogInf
	RspMsg  []byte
}

func AppInit(filename string) (*BaseApp, error) {
	var err error
	app := new(BaseApp)

	err = yamlcfg.LoadCfg(filename, app)
	if err != nil {
		return nil, fmt.Errorf("加载应用配置文件失败:%s", err)
	}
	app.lg = log.GetLog()
	return app, nil
}

func (t *BaseApp) Run(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	msgbody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		t.lg.Error("获取请求报文失败", err)
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	t.lg.Infof("请求报文：%s", string(msgbody))

	err = t.DownloadDzFile(msgbody)
	if err != nil {
		t.lg.Error("对账单处理失败", err)
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(t.RspMsg)
	t.lg.Info("对账文件发送成功:", time.Now().Format("20060102"))

}
