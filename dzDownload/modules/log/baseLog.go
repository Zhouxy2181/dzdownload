package log

import (
	"dzDownload/modules/yamlcfg"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.yietech.com/zzb0905/logrus_mate"
	_ "gitlab.yietech.com/zzb0905/logrus_mate/hooks/expander"
	_ "gitlab.yietech.com/zzb0905/logrus_mate/hooks/file"
	"path/filepath"
	"runtime"
)

type LogInf struct {
	*logrus.Logger
}

type LogCfg struct {
	FileName string `yaml:"FILENAME"`
}

type BaseLog struct {
	Cfg LogCfg `yaml:"LOGCFG"`
	*logrus_mate.LogrusMate
}

var instance BaseLog

func LogInit(filename string) error {
	var err error

	fName, err := filepath.Abs(filename)
	if err != nil {
		return err
	}
	err = yamlcfg.LoadCfg(fName, &instance)
	if err != nil {
		return fmt.Errorf("加载日志配置文件失败:%s", err)
	}
	fmt.Println("加载日志配置文件:", instance.Cfg.FileName)

	instance.LogrusMate, err = logrus_mate.NewLogrusMate(
		logrus_mate.ConfigFile(instance.Cfg.FileName))
	if err != nil {
		return err
	}
	if instance.LogrusMate == nil {
		fmt.Errorf("日志配置文件[%s]加载失败", instance.Cfg.FileName)
	}

	return nil
}

func NewLogger(section ...string) *LogInf {
	l := new(LogInf)
	var sec string
	if len(section) > 0 {
		sec = section[0]
	} else {
		sec = "default"
	}
	l.Logger = instance.Logger(sec)
	if l.Logger == nil {
		panic(fmt.Sprintf("日志配置信息[%s]不存在", sec))
		return nil
	}

	return l
}

type LogEntry struct {
	*logrus.Entry
}

func (t *LogInf) SetLog() *LogEntry {
	le := new(LogEntry)
	_, path, line, ok := runtime.Caller(2)
	if ok {
		_, file := filepath.Split(path)
		le.Entry = t.WithField(fmt.Sprintf("[%s", file), fmt.Sprintf("%d]", line))
	}
	return le
}

func GetLog() *LogInf {
	lg := NewLogger("default")
	return lg
}
