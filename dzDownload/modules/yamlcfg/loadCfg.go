package yamlcfg

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"path/filepath"
)

func LoadCfg(filename string, cfginf interface{}) error {
	fp, err := filepath.Abs(filename)
	if err != nil {
		return err
	}
	cfgdata, err := ioutil.ReadFile(fp)
	if err != nil {
		return err
	}
	err = yaml.Unmarshal(cfgdata, cfginf)
	if err != nil {
		return err
	}

	return nil
}
