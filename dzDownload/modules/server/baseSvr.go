package server

import (
	"crypto/tls"
	"dzDownload/modules/yamlcfg"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"sourcegraph.com/sourcegraph/appdash"
	"sourcegraph.com/sourcegraph/appdash/httptrace"
	"strings"
)

type SvrCfg struct {
	Debug       bool   `yaml:"DEBUG"`
	CollectAddr string `yaml:"COLLECTADDR"`
	ServerAddr  string `yaml:"SERVERADDR"`
	ServerUrl   string `yaml:"SERVERURL"`
	CertFile    string `yaml:"CERTFILE"`
	KeyFile     string `yaml:"KEYFILE"`
}

type BaseSvr struct {
	*SvrCfg `yaml:"HTTPCFG"`
	tracemw func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc)
	Route   *httprouter.Router
}

func SvrInit(filename string) (svr *BaseSvr, err error) {
	svr = new(BaseSvr)

	err = yamlcfg.LoadCfg(filename, svr)
	if err != nil {
		return nil, fmt.Errorf("加载服务配置文件失败:%s", err)
	}

	if svr.Debug {
		var col *appdash.RemoteCollector
		if strings.HasPrefix(svr.CollectAddr, "https") {
			col = appdash.NewTLSRemoteCollector(svr.CollectAddr, &tls.Config{InsecureSkipVerify: true})
		} else {
			col = appdash.NewRemoteCollector(svr.CollectAddr)
		}
		col.Debug = svr.Debug
		svr.tracemw = httptrace.Middleware(col, &httptrace.MiddlewareConfig{})
	}

	svr.Route = httprouter.New()
	return
}

func (t *BaseSvr) AddHandle(name string, svr IService, method string) {
	t.Route.Handle(method, name, func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		svr.Run(w, r, ps)
	})
}

func (t *BaseSvr) Run() {
	var hand http.Handler

	if t.Debug {
		hand = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			t.tracemw(w, r, t.Route.ServeHTTP)
		})
	} else {
		hand = t.Route
	}

	if t.CertFile != "" && t.KeyFile != "" {
		log.Println("start https: " + t.ServerAddr)
		go log.Fatal(http.ListenAndServeTLS(t.ServerAddr, t.CertFile, t.KeyFile, hand))
	} else {
		log.Println("start http: " + t.ServerAddr)
		go log.Fatal(http.ListenAndServe(t.ServerAddr, hand))
	}
}

type IService interface {
	Run(w http.ResponseWriter, r *http.Request, ps httprouter.Params)
}
