package security

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"dzDownload/modules/log"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
)

const (
	RSA_PRIV_BEGIN = "-----BEGIN RSA PRIVATE KEY-----\n"
	RSA_PRIV_END   = "\n-----END RSA PRIVATE KEY-----"
	RSA_PUB_BEGIN  = "-----BEGIN PUBLIC KEY-----\n"
	RSA_PUB_END    = "\n-----END PUBLIC KEY-----"
)

type RSAKey struct {
	PublicKey  *rsa.PublicKey
	PrivateKey *rsa.PrivateKey
}

func NewRsaInMap(pri, pub string) (*RSAKey, error) {
	var err error
	var ok bool
	r := new(RSAKey)

	prikeyBuf := RSA_PRIV_BEGIN + pri + RSA_PRIV_END
	pubkeyBuf := RSA_PUB_BEGIN + pub + RSA_PUB_END

	if pri != "" {
		priblock, _ := pem.Decode([]byte(prikeyBuf))
		if priblock == nil {
			return nil, fmt.Errorf("读取rsa私钥文件为空[%s]", pri)
		}
		r.PrivateKey, err = x509.ParsePKCS1PrivateKey(priblock.Bytes)
		if err != nil {
			return nil, fmt.Errorf("ParsePKCS1PrivateKey err:%s", err)
		}
	} else if pub != "" {
		pubblock, _ := pem.Decode([]byte(pubkeyBuf))
		if pubblock == nil {
			return nil, fmt.Errorf("读取rsa公钥文件为空[%s]", pri)
		}
		pubKey, err := x509.ParsePKIXPublicKey(pubblock.Bytes)
		if err != nil {
			return nil, fmt.Errorf("ParsePKCS1PublicKey err:%s", err)
		}
		r.PublicKey, ok = pubKey.(*rsa.PublicKey)
		if !ok {
			return nil, fmt.Errorf("Value returned from ParsePKIXPublicKey was not an RSA public key")
		}
	} else {
		return nil, errors.New("密钥不合法")
	}

	return r, nil
}

func (t *RSAKey) Verfy(data, sign string) (bool, error) {
	lg := log.GetLog()
	signVery, err := base64.StdEncoding.DecodeString(sign)
	if err != nil {
		lg.Error("base64 decode 失败", sign)
		return false, err
	}

	h := sha1.New()
	h.Write([]byte(data))
	digest := h.Sum(nil)

	err = rsa.VerifyPKCS1v15(t.PublicKey, crypto.SHA1, digest, signVery)
	if err != nil {
		lg.Error("rsa.Verify sha1 失败", err)
		return false, err
	}

	return true, nil
}
