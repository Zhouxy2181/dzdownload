package dbxorm

import (
	"dzDownload/modules/yamlcfg"
	"fmt"
	"github.com/go-xorm/xorm"
	_ "github.com/mattn/go-oci8"
	"path/filepath"
	"time"
	"xorm.io/core"
)

type DbCfg struct {
	Dbaddr      string `yaml:"DBADDR"`
	DbName      string `yaml:"DBNAME"`
	DbUser      string `yaml:"DBUSER"`
	DbPasswd    string `yaml:"DBPASSWD"`
	MaxIdleConn int    `yaml:"MAXIDLECONN"`
	MaxOpenConn int    `yaml:"MAXOPENCONN"`
	MaxLifeTime int    `yaml:"MAXLIFETIME"`
	Debug       bool   `yaml:"DEBUG"`
}

type DbInstance struct {
	Dbcfg  DbCfg `yaml:"DBCFG"`
	Engine *xorm.Engine
}

var Dbins *DbInstance

func DbInit(filename string) error {
	var err error

	fpath, err := filepath.Abs(filename)
	if err != nil {
		return err
	}
	Dbins = new(DbInstance)
	err = yamlcfg.LoadCfg(fpath, Dbins)
	if err != nil {
		return fmt.Errorf("加载数据库配置失败:%s", err)
	}

	dbstr := Dbins.Dbcfg.DbUser + "/" + Dbins.Dbcfg.DbPasswd + "@" + Dbins.Dbcfg.Dbaddr + "/" +
		Dbins.Dbcfg.DbName
	Dbins.Engine, err = xorm.NewEngine("oci8", dbstr)
	if err != nil {
		return fmt.Errorf("连接数据库失败", err)
	}

	Dbins.Engine.SetTableMapper(core.SameMapper{})
	Dbins.Engine.SetColumnMapper(core.SameMapper{})
	Dbins.Engine.SetMaxIdleConns(Dbins.Dbcfg.MaxIdleConn)
	Dbins.Engine.SetMaxOpenConns(Dbins.Dbcfg.MaxOpenConn)
	Dbins.Engine.SetConnMaxLifetime(time.Duration(Dbins.Dbcfg.MaxLifeTime) * time.Second)
	Dbins.Engine.ShowSQL(Dbins.Dbcfg.Debug)

	return nil
}

func GetDbInstance() *xorm.Engine {
	return Dbins.Engine
}
